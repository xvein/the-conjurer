package com.legacy.conjurer_illager;

import com.legacy.conjurer_illager.client.render.IllagerEntityRendering;
import com.legacy.conjurer_illager.data.ConjurerDataGen;
import com.legacy.conjurer_illager.registry.IllagerEntityTypes;
import com.legacy.conjurer_illager.registry.IllagerProcessors;
import com.legacy.conjurer_illager.registry.IllagerStructures;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.DyeableLeatherItem;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.RegisterColorHandlersEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(TheConjurerMod.MODID)
public class TheConjurerMod
{
	public static final String NAME = "The Conjurer";
	public static final String MODID = "conjurer_illager";
	public static final MLSupporter SUPPORTERS = new MLSupporter(MODID);

	public TheConjurerMod()
	{
		/*ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, IllagerConfig.COMMON_SPEC);*/

		IEventBus mod = FMLJavaModLoadingContext.get().getModEventBus();
		IEventBus forge = MinecraftForge.EVENT_BUS;

		DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () ->
		{
			IllagerEntityRendering.init();
			mod.addListener((RegisterColorHandlersEvent.Item event) -> event.register((stack, layer) -> layer == 0 ? ((DyeableLeatherItem) stack.getItem()).getColor(stack) : -1, IllagerRegistry.CONJURER_HAT.get()));
			mod.addListener(com.legacy.conjurer_illager.registry.IllagerParticles.Register::registerParticleFactories);
		});

		IllagerStructures.init();
		RegistrarHandler.registerHandlers(MODID, mod, IllagerProcessors.HANDLER);

		mod.addListener(TheConjurerMod::commonInit);
		mod.addListener(IllagerEntityTypes::onAttributesRegistered);
		mod.addListener(EventPriority.LOWEST, IllagerEntityTypes::registerPlacements);
		mod.register(IllagerRegistry.class);
		forge.register(IllagerEvents.class);
		
		mod.register(ConjurerDataGen.class);
	}

	private static void commonInit(FMLCommonSetupEvent event)
	{
		SUPPORTERS.refresh();
	}

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(MODID, name);
	}

	public static String find(String name)
	{
		return new String(MODID + ":" + name);
	}
}
