package com.legacy.conjurer_illager;

import java.util.Locale;

import javax.annotation.Nullable;

import com.legacy.conjurer_illager.entity.ConjurerEntity;
import com.legacy.conjurer_illager.registry.IllagerStructures;

import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.util.RandomSource;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.ai.goal.AvoidEntityGoal;
import net.minecraft.world.entity.animal.Rabbit;
import net.minecraft.world.entity.npc.AbstractVillager;
import net.minecraft.world.entity.npc.Villager;
import net.minecraft.world.entity.npc.VillagerProfession;
import net.minecraft.world.entity.npc.VillagerTrades;
import net.minecraft.world.entity.npc.WanderingTrader;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.MapItem;
import net.minecraft.world.item.trading.MerchantOffer;
import net.minecraft.world.level.saveddata.maps.MapDecoration;
import net.minecraft.world.level.saveddata.maps.MapItemSavedData;
import net.minecraftforge.event.entity.EntityJoinLevelEvent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.MobEffectEvent;
import net.minecraftforge.event.village.VillagerTradesEvent;
import net.minecraftforge.eventbus.api.Event.Result;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class IllagerEvents
{
	@SubscribeEvent
	public static void onEntityJoinWorld(EntityJoinLevelEvent event)
	{
		if (event.getEntity() instanceof Villager || event.getEntity() instanceof WanderingTrader)
		{
			AbstractVillager villager = (AbstractVillager) event.getEntity();
			villager.goalSelector.addGoal(1, new AvoidEntityGoal<ConjurerEntity>(villager, ConjurerEntity.class, 8.0F, 0.6D, 0.6D));
		}
	}

	@SubscribeEvent
	public static void onPotionEffectGiven(MobEffectEvent.Applicable event)
	{
		if (event.getEffectInstance().getEffect() == MobEffects.BAD_OMEN && event.getEntity().getItemBySlot(EquipmentSlot.HEAD).getItem() == IllagerRegistry.CONJURER_HAT.get())
			event.setResult(Result.DENY);
	}

	@SubscribeEvent
	public static void onLivingAttack(LivingAttackEvent event)
	{
		if (event.getAmount() > 0 && event.getSource().getDirectEntity() instanceof Rabbit)
			event.getSource().getDirectEntity().playSound(SoundEvents.RABBIT_ATTACK, 1.0F, (event.getEntity().level.random.nextFloat() - event.getEntity().level.random.nextFloat()) * 0.2F + 1.0F);
	}

	@SubscribeEvent
	public static void onLivingDamage(LivingDamageEvent event)
	{
		if (event.getSource().isMagic() && event.getEntity().getItemBySlot(EquipmentSlot.HEAD).getItem() == IllagerRegistry.CONJURER_HAT.get())
			event.setAmount(event.getAmount() * 0.70F);
	}

	@SubscribeEvent
	public static void onVillagerTradesAssigned(VillagerTradesEvent event)
	{
		try
		{
			if (event.getType() == VillagerProfession.CARTOGRAPHER)
				event.getTrades().get(2).add(new IllagerEvents.EmeraldForMapTrade(11, MapDecoration.Type.RED_X, 15, 5));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	static class EmeraldForMapTrade implements VillagerTrades.ItemListing
	{
		private final int count;
		private final MapDecoration.Type mapDecorationType;
		private final int maxUses;
		private final int xpValue;

		public EmeraldForMapTrade(int emeraldCountIn, MapDecoration.Type mapTypeIn, int maxUsesIn, int experienceValueIn)
		{
			this.count = emeraldCountIn;
			this.mapDecorationType = mapTypeIn;
			this.maxUses = maxUsesIn;
			this.xpValue = experienceValueIn;
		}

		@Nullable
		public MerchantOffer getOffer(Entity trader, RandomSource rand)
		{
			if (!(trader.level instanceof ServerLevel))
			{
				return null;
			}
			else
			{
				ServerLevel serverworld = (ServerLevel) trader.level;
				BlockPos blockpos = serverworld.findNearestMapStructure(IllagerStructures.THEATERS, trader.blockPosition(), 100, true);
				if (blockpos != null)
				{
					ItemStack itemstack = MapItem.create(serverworld, blockpos.getX(), blockpos.getZ(), (byte) 2, true, true);
					MapItem.renderBiomePreviewMap(serverworld, itemstack);
					MapItemSavedData.addTargetDecoration(itemstack, blockpos, "+", this.mapDecorationType);
					itemstack.setHoverName(Component.translatable("filled_map." + IllagerStructures.THEATER.getRegistryName().toString().toLowerCase(Locale.ROOT)));
					return new MerchantOffer(new ItemStack(Items.EMERALD, this.count), new ItemStack(Items.COMPASS), itemstack, this.maxUses, this.xpValue, 0.2F);
				}
				else
				{
					return null;
				}
			}
		}
	}
}