package com.legacy.conjurer_illager.item;

import java.util.function.Supplier;

import com.legacy.conjurer_illager.TheConjurerMod;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.RecordItem;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class IllagerRecordItem extends RecordItem
{
	private final String artistName, songName;

	public IllagerRecordItem(int comparatorValue, String artist, String songName, Supplier<SoundEvent> soundSupplier, Properties builder)
	{
		super(comparatorValue, soundSupplier, builder, 8);
		
		this.artistName = artist;
		this.songName = songName;
	}

	@Override
	public String getDescriptionId(ItemStack stack)
	{
		return String.format("item.%s.music_disc", TheConjurerMod.MODID);
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public MutableComponent getDisplayName()
	{
		return Component.literal(this.artistName + " - " + this.songName);
	}
}
