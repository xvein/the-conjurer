package com.legacy.conjurer_illager.item;

import com.legacy.conjurer_illager.entity.ThrowingCardEntity;
import com.legacy.conjurer_illager.registry.IllagerEntityTypes;

import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.stats.Stats;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class ThrowableCardItem extends Item
{
	public ThrowableCardItem(Item.Properties builder)
	{
		super(builder);
	}

	@Override
	public InteractionResultHolder<ItemStack> use(Level level, Player player, InteractionHand hand)
	{
		ItemStack itemstack = player.getItemInHand(hand);
		level.playSound((Player) null, player.getX(), player.getY(), player.getZ(), SoundEvents.BOOK_PAGE_TURN, SoundSource.NEUTRAL, 1.0F, 1.0F);

		if (!level.isClientSide)
		{
			ThrowingCardEntity card = new ThrowingCardEntity(IllagerEntityTypes.THROWING_CARD, player, level);
			card.shootFromRotation(player, player.getXRot(), player.getYRot(), 1.0F, 1F, 3F); // shoot
			card.setCardType(level.random.nextInt(2));
			level.addFreshEntity(card);
		}

		player.awardStat(Stats.ITEM_USED.get(this));

		if (!player.isCreative())
			itemstack.shrink(1);

		return InteractionResultHolder.success(itemstack);
	}

	@Override
	public Component getName(ItemStack stack)
	{
		return Component.translatable("entity.conjurer_illager.throwing_card");
	}
}