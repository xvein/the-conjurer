package com.legacy.conjurer_illager.item;

import com.legacy.conjurer_illager.entity.BouncingBallEntity;
import com.legacy.conjurer_illager.registry.IllagerEntityTypes;

import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.stats.Stats;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class ThrowableBallItem extends Item
{
	public ThrowableBallItem(Item.Properties builder)
	{
		super(builder);
	}

	@Override
	public InteractionResultHolder<ItemStack> use(Level level, Player player, InteractionHand hand)
	{
		ItemStack itemstack = player.getItemInHand(hand);
		level.playSound((Player) null, player.getX(), player.getY(), player.getZ(), SoundEvents.SNOWBALL_THROW, SoundSource.NEUTRAL, 0.5F, 0.4F / (level.random.nextFloat() * 0.4F + 0.8F));

		if (!level.isClientSide)
		{
			BouncingBallEntity ball = new BouncingBallEntity(IllagerEntityTypes.BOUNCING_BALL, player, level);
			ball.shootFromRotation(player, player.getXRot(), player.getYRot(), 1.0F, 1F, 3F); // shoot
			ball.setBallType(level.random.nextInt(5));
			level.addFreshEntity(ball);
		}

		player.awardStat(Stats.ITEM_USED.get(this));

		if (!player.isCreative())
			itemstack.shrink(1);

		return InteractionResultHolder.success(itemstack);
	}
	
	@Override
	public Component getName(ItemStack stack)
	{
		return Component.translatable("entity.conjurer_illager.bouncing_ball");
	}
}