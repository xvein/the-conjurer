package com.legacy.conjurer_illager.item;

import java.util.List;
import java.util.function.Consumer;

import javax.annotation.Nullable;

import com.legacy.conjurer_illager.TheConjurerMod;

import net.minecraft.ChatFormatting;
import net.minecraft.client.model.HumanoidModel;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.DyeableArmorItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.extensions.common.IClientItemExtensions;

public class ConjurerHatItem extends DyeableArmorItem
{
	public ConjurerHatItem(ArmorMaterial materialIn, EquipmentSlot slot, Properties builder)
	{
		super(materialIn, slot, builder);
	}

	@Override
	public int getColor(ItemStack stack)
	{
		CompoundTag compoundnbt = stack.getTagElement("display");
		return compoundnbt != null && compoundnbt.contains("color", 99) ? compoundnbt.getInt("color") : 0xE79E4A;
	}

	@Override
	@OnlyIn(Dist.CLIENT)
	public void appendHoverText(ItemStack stack, @Nullable Level worldIn, List<Component> lore, TooltipFlag flagIn)
	{
		lore.add(Component.translatable(String.format("gui.%s.item.conjurer_hat.desc", TheConjurerMod.MODID)).withStyle(ChatFormatting.GOLD));
	}

	@Override
	public void initializeClient(Consumer<IClientItemExtensions> consumer)
	{
		consumer.accept(new IClientItemExtensions()
		{
			@Override
			public HumanoidModel<?> getHumanoidArmorModel(LivingEntity entityLiving, ItemStack itemStack, EquipmentSlot armorSlot, HumanoidModel<?> _default)
			{
				return new com.legacy.conjurer_illager.client.model.ConjurerHatModel<LivingEntity>(net.minecraft.client.Minecraft.getInstance().getEntityModels().bakeLayer(com.legacy.conjurer_illager.client.IllagerRenderRefs.CONJURER_HAT));
			}
		});
	}
}