package com.legacy.conjurer_illager;

import com.legacy.conjurer_illager.item.ConjurerHatItem;
import com.legacy.conjurer_illager.item.IllagerArmorMaterial;
import com.legacy.conjurer_illager.item.IllagerRecordItem;
import com.legacy.conjurer_illager.item.ThrowableBallItem;
import com.legacy.conjurer_illager.item.ThrowableCardItem;
import com.legacy.conjurer_illager.registry.IllagerEntityTypes;
import com.legacy.conjurer_illager.registry.IllagerSounds;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.decoration.PaintingVariant;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Rarity;
import net.minecraftforge.common.ForgeSpawnEggItem;
import net.minecraftforge.common.util.Lazy;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.registries.RegisterEvent;

public class IllagerRegistry
{
	public static final Lazy<Item> CONJURER_HAT = Lazy.of(() -> new ConjurerHatItem(IllagerArmorMaterial.CONJURER_HAT, EquipmentSlot.HEAD, new Item.Properties()));
	public static final Lazy<Item> CONJURER_SPAWN_EGG = Lazy.of(() -> new ForgeSpawnEggItem(() -> IllagerEntityTypes.CONJURER, 0x71265b, 0xe79e4a, new Item.Properties()));
	public static final Lazy<Item> THROWABLE_BALL = Lazy.of(() -> new ThrowableBallItem(new Item.Properties().stacksTo(8)));
	public static final Lazy<Item> THROWING_CARD = Lazy.of(() -> new ThrowableCardItem(new Item.Properties().stacksTo(52)));

	/*public static final Lazy<Item> CONJURED_CHAOS_RECORD = Lazy.of(() -> new RecordItem(7, () -> IllagerSounds.RECORD_CONJURED_CHAOS, new Item.Properties().tab(CreativeModeTab.TAB_MISC)));*/
	public static final Lazy<Item> DELVE_DEEPER_RECORD = Lazy.of(() -> new IllagerRecordItem(13, "Jesterguy", "Delve Deeper", () -> IllagerSounds.RECORD_DELVE_DEEPER, new Item.Properties().rarity(Rarity.RARE).stacksTo(1)));

	public static final PaintingVariant THEATER_PAINTING = new PaintingVariant(32, 32);

	/*@SubscribeEvent
	public static void onRegisterStructures(Register<StructureFeature<?>> event)
	{
		IllagerStructures.init(event);
	}*/

	@SubscribeEvent
	public static void onRegistry(RegisterEvent event)
	{
		if (event.getRegistryKey().equals(Registries.ENTITY_TYPE))
		{
			IllagerEntityTypes.init(event);
		}
		else if (event.getRegistryKey().equals(Registries.ITEM))
		{
			event.register(Registries.ITEM, TheConjurerMod.locate("conjurer_hat"), CONJURER_HAT);
			event.register(Registries.ITEM, TheConjurerMod.locate("conjurer_spawn_egg"), CONJURER_SPAWN_EGG);

			event.register(Registries.ITEM, TheConjurerMod.locate("throwable_ball"), THROWABLE_BALL);
			event.register(Registries.ITEM, TheConjurerMod.locate("throwing_card"), THROWING_CARD);

			/*event.register(Registries.ITEM, TheConjurerMod.locate("music_disc_conjured_chaos"), CONJURED_CHAOS_RECORD);*/
			event.register(Registries.ITEM, TheConjurerMod.locate("music_disc_delve_deeper"), DELVE_DEEPER_RECORD);
		}
		else if (event.getRegistryKey().equals(Registries.SOUND_EVENT))
			IllagerSounds.init();
		else if (event.getRegistryKey().equals(Registries.PAINTING_VARIANT))
			event.register(Registries.PAINTING_VARIANT, TheConjurerMod.locate("theater"), () -> THEATER_PAINTING);
		else if (event.getRegistryKey().equals(Registries.PARTICLE_TYPE))
			com.legacy.conjurer_illager.registry.IllagerParticles.init(event);
	}
}