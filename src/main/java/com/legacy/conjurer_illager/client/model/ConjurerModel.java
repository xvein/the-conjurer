package com.legacy.conjurer_illager.client.model;

import com.legacy.conjurer_illager.entity.ConjurerEntity;

import net.minecraft.client.model.IllagerModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.monster.AbstractIllager;

public class ConjurerModel<T extends ConjurerEntity> extends IllagerModel<T>
{
	protected final ModelPart hatTop;
	protected final ModelPart hatBottom;

	protected final ModelPart heldHatBottom;
	protected final ModelPart heldHatTop;

	protected final ModelPart cape;

	protected final ModelPart bowTie;

	protected final ModelPart head;
	protected final ModelPart body;
	protected final ModelPart rightLeg;
	protected final ModelPart leftLeg;
	protected final ModelPart rightArm;
	protected final ModelPart leftArm;

	public ConjurerModel(ModelPart model)
	{
		super(model);

		this.head = model.getChild("head");
		this.body = model.getChild("body");
		this.rightLeg = model.getChild("right_leg");
		this.leftLeg = model.getChild("left_leg");
		this.rightArm = model.getChild("right_arm");
		this.leftArm = model.getChild("left_arm");

		this.hatTop = model.getChild("hat_top");
		this.hatBottom = model.getChild("hat_bottom");
		this.heldHatBottom = model.getChild("held_hat_bottom");
		this.heldHatTop = model.getChild("held_hat_top");
		this.cape = model.getChild("cape");
		this.bowTie = model.getChild("bow_tie");
	}

	public static LayerDefinition createBodyLayer(CubeDeformation size)
	{
		MeshDefinition mesh = new MeshDefinition();
		PartDefinition root = mesh.getRoot();

		var head = root.addOrReplaceChild("head", CubeListBuilder.create().texOffs(0, 0).addBox(-4.0F, -10.0F, -4.0F, 8.0F, 10.0F, 8.0F, size), PartPose.ZERO);
		head.addOrReplaceChild("hat", CubeListBuilder.create().texOffs(32, 0).addBox(-4.0F, -10.0F, -4.0F, 8.0F, 12.0F, 8.0F, new CubeDeformation(0.45F)), PartPose.ZERO);
		head.addOrReplaceChild("nose", CubeListBuilder.create().texOffs(24, 0).addBox(-1.0F, -1.0F, -6.0F, 2.0F, 4.0F, 2.0F), PartPose.offset(0.0F, -2.0F, 0.0F));

		root.addOrReplaceChild("body", CubeListBuilder.create().texOffs(16, 20).addBox(-4.0F, 0.0F, -3.0F, 8.0F, 12.0F, 6.0F).texOffs(0, 38).addBox(-4.0F, 0.0F, -3.0F, 8.0F, 20.0F, 6.0F, new CubeDeformation(0.5F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		var arms = root.addOrReplaceChild("arms", CubeListBuilder.create().texOffs(44, 22).addBox(-8.0F, -2.0F, -2.0F, 4.0F, 8.0F, 4.0F).texOffs(40, 38).addBox(-4.0F, 2.0F, -2.0F, 8.0F, 4.0F, 4.0F), PartPose.offsetAndRotation(0.0F, 3.0F, -1.0F, -0.75F, 0.0F, 0.0F));
		arms.addOrReplaceChild("left_shoulder", CubeListBuilder.create().texOffs(44, 22).mirror().addBox(4.0F, -2.0F, -2.0F, 4.0F, 8.0F, 4.0F), PartPose.ZERO);
		root.addOrReplaceChild("right_leg", CubeListBuilder.create().texOffs(0, 22).addBox(-2.0F, 0.0F, -2.0F, 4.0F, 12.0F, 4.0F), PartPose.offset(-2.0F, 12.0F, 0.0F));
		root.addOrReplaceChild("left_leg", CubeListBuilder.create().texOffs(0, 22).mirror().addBox(-2.0F, 0.0F, -2.0F, 4.0F, 12.0F, 4.0F), PartPose.offset(2.0F, 12.0F, 0.0F));
		root.addOrReplaceChild("right_arm", CubeListBuilder.create().texOffs(40, 46).addBox(-3.0F, -2.0F, -2.0F, 4.0F, 12.0F, 4.0F), PartPose.offset(-5.0F, 2.0F, 0.0F));
		root.addOrReplaceChild("left_arm", CubeListBuilder.create().texOffs(40, 46).mirror().addBox(-1.0F, -2.0F, -2.0F, 4.0F, 12.0F, 4.0F), PartPose.offset(5.0F, 2.0F, 0.0F));

		root.addOrReplaceChild("hat_top", CubeListBuilder.create().texOffs(96, 0).addBox(-4.0F, -17.0F, -4.0F, 8, 8, 8, new CubeDeformation(0.01F)), PartPose.ZERO);
		root.addOrReplaceChild("hat_bottom", CubeListBuilder.create().texOffs(84, 16).addBox(-5.5F, -9.0F, -5.5F, 11, 2, 11, new CubeDeformation(0.01F)), PartPose.ZERO);

		root.addOrReplaceChild("held_hat_top", CubeListBuilder.create().texOffs(70, 48).addBox(-0.5F, 10.0F, -4.0F, 8.0F, 8.0F, 8.0F), PartPose.offset(-5.0F, 2.0F, 0.0F));
		root.addOrReplaceChild("held_hat_bottom", CubeListBuilder.create().texOffs(102, 42).addBox(-2.5F, 8.5F, -5.5F, 2.0F, 11.0F, 11.0F), PartPose.offset(-5.0F, 2.0F, 0.0F));

		root.addOrReplaceChild("bow_tie", CubeListBuilder.create().texOffs(32, 0).addBox(-3.5F, 0.0F, -3.3F, 7, 4, 0), PartPose.ZERO);
		root.addOrReplaceChild("cape", CubeListBuilder.create().texOffs(72, 0).addBox(-5.0F, 0.0F, -1.0F, 10.0F, 16.0F, 1.0F), PartPose.ZERO);

		return LayerDefinition.create(mesh, 128, 64);
	}

	public void setRotateAngle(ModelPart modelRenderer, float x, float y, float z)
	{
		modelRenderer.xRot = x;
		modelRenderer.yRot = y;
		modelRenderer.zRot = z;
	}

	@Override
	public void setupAnim(T entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		super.setupAnim(entityIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);

		this.head.setPos(0.0F, 0.0F, 0.0F);

		boolean spellcasting = entityIn.getArmPose() == AbstractIllager.IllagerArmPose.SPELLCASTING;
		boolean celebrating = entityIn.getArmPose() == AbstractIllager.IllagerArmPose.CELEBRATING;

		this.heldHatBottom.visible = spellcasting || celebrating;
		this.heldHatTop.visible = spellcasting || celebrating;
		this.hatBottom.visible = !spellcasting && !celebrating;
		this.hatTop.visible = !spellcasting && !celebrating;

		this.hatBottom.copyFrom(this.head);
		this.hatTop.copyFrom(this.head);

		this.bowTie.copyFrom(this.body);

		this.cape.xRot = 0.1F + limbSwingAmount * 0.6F + (celebrating ? 0.5F : 0.0F);
		this.cape.z = 4.0F + (celebrating ? -7.0F : 0.0F);
		this.cape.y = -0.6F + (celebrating ? -2.0F : 0.0F);

		if (celebrating)
		{
			this.body.xRot = 0.5F;
			this.body.z = -6.5F;

			this.head.z = -7.5F;
			this.head.xRot = 0.7F + Mth.cos(ageInTicks * 0.2F) * 0.1F;

			this.rightArm.z = -5.5F;
			this.rightArm.x = -5.0F;
			this.rightArm.xRot = 0;
			this.rightArm.zRot = 2.0F + Mth.cos(ageInTicks * 0.1F) * 0.2F;
			this.rightArm.yRot = 0.0F;

			this.leftArm.z = -5.5F;
			this.leftArm.x = 5.0F;

			// forward back
			this.leftArm.xRot = 0.6F;
			// left right
			this.leftArm.zRot = 0.0F;

			this.leftArm.yRot = 0.0F;

			this.heldHatBottom.copyFrom(this.rightArm);
			this.heldHatTop.copyFrom(this.rightArm);
		}
		else
		{
			this.heldHatBottom.copyFrom(rightArm);
			this.heldHatTop.copyFrom(rightArm);

			this.body.xRot = 0.0F;
			this.body.z = 0.0F;

			head.z = 0.0F;
		}

		if (entityIn.getArmPose() == AbstractIllager.IllagerArmPose.CROSSBOW_HOLD)
			this.leftArm.yRot = Mth.cos(ageInTicks * 2.0F) * 0.2F;
	}
}
