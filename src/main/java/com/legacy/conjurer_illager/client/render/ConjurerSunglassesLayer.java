package com.legacy.conjurer_illager.client.render;

import com.legacy.conjurer_illager.TheConjurerMod;
import com.legacy.conjurer_illager.client.model.ConjurerModel;
import com.legacy.conjurer_illager.entity.ConjurerEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.model.ArmedModel;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.RenderLayerParent;
import net.minecraft.client.renderer.entity.layers.RenderLayer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ConjurerSunglassesLayer<T extends ConjurerEntity, M extends ConjurerModel<T> & ArmedModel> extends RenderLayer<T, M>
{
	private static final ResourceLocation TEXTURE = TheConjurerMod.locate("textures/entity/sunglasses.png");

	private final ConjurerModel<ConjurerEntity> illagerModel;

	// 0.3F
	public ConjurerSunglassesLayer(RenderLayerParent<T, M> renderer, ConjurerModel<ConjurerEntity> model)
	{
		super(renderer);
		
		this.illagerModel = model;
	}

	@SuppressWarnings("unchecked")
	public void render(PoseStack matrixStackIn, MultiBufferSource bufferIn, int packedLightIn, T entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch)
	{
		if (!entitylivingbaseIn.isInvisible() && entitylivingbaseIn.hasCustomName() && ("Vamacheron".equals(entitylivingbaseIn.getName().getContents()) || "Vamacher0n".equals(entitylivingbaseIn.getName().getContents())))
		{
			this.getParentModel().copyPropertiesTo((ConjurerModel<T>) this.illagerModel);
			this.illagerModel.prepareMobModel(entitylivingbaseIn, limbSwing, limbSwingAmount, partialTicks);
			this.illagerModel.setupAnim(entitylivingbaseIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
			VertexConsumer ivertexbuilder = bufferIn.getBuffer(RenderType.entityTranslucentCull(TEXTURE));
			this.illagerModel.renderToBuffer(matrixStackIn, ivertexbuilder, packedLightIn, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
		}
	}
}