package com.legacy.conjurer_illager.client.render;

import com.legacy.conjurer_illager.TheConjurerMod;
import com.legacy.conjurer_illager.client.IllagerRenderRefs;
import com.legacy.conjurer_illager.client.model.ThrowingCardModel;
import com.legacy.conjurer_illager.entity.ThrowingCardEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Axis;

import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ThrowingCardRenderer extends EntityRenderer<ThrowingCardEntity>
{
	private static final ResourceLocation[] CARDS = new ResourceLocation[] { TheConjurerMod.locate("textures/entity/throwing_card/red_throwing_card.png"), TheConjurerMod.locate("textures/entity/throwing_card/black_throwing_card.png"), TheConjurerMod.locate("textures/entity/throwing_card/blue_throwing_card.png") };
	private final ThrowingCardModel model;

	public ThrowingCardRenderer(EntityRendererProvider.Context context)
	{
		super(context);
		
		this.model = new ThrowingCardModel(context.bakeLayer(IllagerRenderRefs.THROWING_CARD));
	}

	@Override
	public void render(ThrowingCardEntity entityIn, float entityYaw, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int packedLightIn)
	{
		matrixStackIn.pushPose();
		matrixStackIn.translate(0.0D, (double) -0.23F, 0.0D);
		matrixStackIn.scale(0.8F, 0.2F, 0.8F);
		matrixStackIn.mulPose(Axis.YP.rotationDegrees(Mth.lerp(partialTicks, entityIn.yRotO, entityIn.getYRot())));
		model.setupAnim(entityIn, partialTicks, 0.0F, -0.1F, 0.0F, 0.0F);
		VertexConsumer ivertexbuilder = bufferIn.getBuffer(model.renderType(CARDS[entityIn.getCardType()]));
		model.renderToBuffer(matrixStackIn, ivertexbuilder, packedLightIn, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
		matrixStackIn.popPose();
		super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
	}

	@Override
	public ResourceLocation getTextureLocation(ThrowingCardEntity entity)
	{
		return CARDS[entity.getCardType()];
	}
}