package com.legacy.conjurer_illager.client.render;

import org.joml.Matrix3f;
import org.joml.Matrix4f;

import com.legacy.conjurer_illager.TheConjurerMod;
import com.legacy.conjurer_illager.entity.BouncingBallEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Axis;

import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class BouncingBallRenderer extends EntityRenderer<BouncingBallEntity>
{
	private static final ResourceLocation[] BALLS = new ResourceLocation[] { TheConjurerMod.locate("textures/entity/bouncy_ball/stripe_1.png"), TheConjurerMod.locate("textures/entity/bouncy_ball/stripe_2.png"), TheConjurerMod.locate("textures/entity/bouncy_ball/stripes_1.png"), TheConjurerMod.locate("textures/entity/bouncy_ball/stripes_2.png"), TheConjurerMod.locate("textures/entity/bouncy_ball/spots_1.png"), TheConjurerMod.locate("textures/entity/bouncy_ball/spots_2.png") };
	private static final RenderType[] RENDER_TYPES = new RenderType[] { RenderType.entityCutoutNoCull(BALLS[0]), RenderType.entityCutoutNoCull(BALLS[1]), RenderType.entityCutoutNoCull(BALLS[2]), RenderType.entityCutoutNoCull(BALLS[3]), RenderType.entityCutoutNoCull(BALLS[4]), RenderType.entityCutoutNoCull(BALLS[5]) };

	public BouncingBallRenderer(EntityRendererProvider.Context context)
	{
		super(context);
	}

	@Override
	public void render(BouncingBallEntity entityIn, float entityYaw, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int packedLightIn)
	{
		matrixStackIn.pushPose();
		matrixStackIn.scale(0.8F, 0.8F, 0.8F);
		matrixStackIn.mulPose(this.entityRenderDispatcher.cameraOrientation());
		matrixStackIn.mulPose(Axis.YP.rotationDegrees(180.0F));
		PoseStack.Pose matrixstack$entry = matrixStackIn.last();
		Matrix4f matrix4f = matrixstack$entry.pose();
		Matrix3f matrix3f = matrixstack$entry.normal();
		VertexConsumer ivertexbuilder = bufferIn.getBuffer(RENDER_TYPES[entityIn.getBallType()]);
		setupVertex(ivertexbuilder, matrix4f, matrix3f, packedLightIn, 0.0F, 0, 0, 1);
		setupVertex(ivertexbuilder, matrix4f, matrix3f, packedLightIn, 1.0F, 0, 1, 1);
		setupVertex(ivertexbuilder, matrix4f, matrix3f, packedLightIn, 1.0F, 1, 1, 0);
		setupVertex(ivertexbuilder, matrix4f, matrix3f, packedLightIn, 0.0F, 1, 0, 0);
		matrixStackIn.popPose();
		super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
	}

	private static void setupVertex(VertexConsumer builder, Matrix4f matrix4f, Matrix3f matrix3f, int p_229045_3_, float p_229045_4_, int p_229045_5_, int p_229045_6_, int p_229045_7_)
	{
		builder.vertex(matrix4f, p_229045_4_ - 0.5F, (float) p_229045_5_ - 0.25F, 0.0F).color(255, 255, 255, 255).uv((float) p_229045_6_, (float) p_229045_7_).overlayCoords(OverlayTexture.NO_OVERLAY).uv2(p_229045_3_).normal(matrix3f, 0.0F, 1.0F, 0.0F).endVertex();
	}

	@Override
	public ResourceLocation getTextureLocation(BouncingBallEntity entity)
	{
		return BALLS[entity.getBallType()];
	}
}