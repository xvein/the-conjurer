package com.legacy.conjurer_illager.client.render;

import com.legacy.conjurer_illager.TheConjurerMod;
import com.legacy.conjurer_illager.client.IllagerRenderRefs;
import com.legacy.conjurer_illager.client.model.ConjurerModel;
import com.legacy.conjurer_illager.entity.ConjurerEntity;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.model.IllagerModel;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.IllagerRenderer;
import net.minecraft.client.renderer.entity.layers.ItemInHandLayer;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ConjurerRenderer<T extends ConjurerEntity> extends IllagerRenderer<T>
{
	private static final ResourceLocation TEXTURE = TheConjurerMod.locate("textures/entity/conjurer_illager.png");

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ConjurerRenderer(EntityRendererProvider.Context context)
	{
		super(context, (IllagerModel<T>) new ConjurerModel<T>(context.bakeLayer(IllagerRenderRefs.CONJURER)), 0.5F);
		this.addLayer(new ConjurerSunglassesLayer(this, new ConjurerModel<T>(context.bakeLayer(IllagerRenderRefs.CONJURER_GLASSES))));
		this.addLayer(new ItemInHandLayer<T, IllagerModel<T>>(this, context.getItemInHandRenderer())
		{
			@Override
			public void render(PoseStack matrixStackIn, MultiBufferSource bufferIn, int packedLightIn, T entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch)
			{
				if (entitylivingbaseIn.isCastingSpell())
					super.render(matrixStackIn, bufferIn, packedLightIn, entitylivingbaseIn, limbSwing, limbSwingAmount, partialTicks, ageInTicks, netHeadYaw, headPitch);
			}
		});
	}

	@Override
	public ResourceLocation getTextureLocation(T entity)
	{
		return TEXTURE;
	}
}