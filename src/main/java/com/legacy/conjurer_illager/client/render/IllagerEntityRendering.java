package com.legacy.conjurer_illager.client.render;

import com.legacy.conjurer_illager.client.IllagerRenderRefs;
import com.legacy.conjurer_illager.client.model.ConjurerHatModel;
import com.legacy.conjurer_illager.client.model.ConjurerModel;
import com.legacy.conjurer_illager.client.model.ThrowingCardModel;
import com.legacy.conjurer_illager.registry.IllagerEntityTypes;

import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

public class IllagerEntityRendering
{
	public static void init()
	{
		IEventBus mod = FMLJavaModLoadingContext.get().getModEventBus();
		mod.addListener(IllagerEntityRendering::initLayers);
		mod.addListener(IllagerEntityRendering::initRenders);
	}

	public static void initLayers(EntityRenderersEvent.RegisterLayerDefinitions event)
	{
		event.registerLayerDefinition(IllagerRenderRefs.CONJURER, () -> ConjurerModel.createBodyLayer(CubeDeformation.NONE));
		event.registerLayerDefinition(IllagerRenderRefs.CONJURER_GLASSES, () -> ConjurerModel.createBodyLayer(new CubeDeformation(0.3F)));

		event.registerLayerDefinition(IllagerRenderRefs.THROWING_CARD, () -> ThrowingCardModel.createLayer());
		
		event.registerLayerDefinition(IllagerRenderRefs.CONJURER_HAT, () -> ConjurerHatModel.createLayer());
	}
	
	public static void initRenders(EntityRenderersEvent.RegisterRenderers event)
	{
		event.registerEntityRenderer(IllagerEntityTypes.CONJURER, ConjurerRenderer::new);
		event.registerEntityRenderer(IllagerEntityTypes.THROWING_CARD, ThrowingCardRenderer::new);
		event.registerEntityRenderer(IllagerEntityTypes.BOUNCING_BALL, BouncingBallRenderer::new);
	}

	/*public static void init()
	{
		register(IllagerEntityTypes.CONJURER, ConjurerRenderer::new);
		register(IllagerEntityTypes.BOUNCING_BALL, BouncingBallRenderer::new);
		register(IllagerEntityTypes.THROWING_CARD, ThrowingCardRenderer::new);
	}*/
}