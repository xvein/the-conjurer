package com.legacy.conjurer_illager.client;

import com.legacy.conjurer_illager.TheConjurerMod;

import net.minecraft.client.model.geom.ModelLayerLocation;

public class IllagerRenderRefs
{
	public static final ModelLayerLocation CONJURER = layer("conjurer");
	public static final ModelLayerLocation CONJURER_GLASSES = layer("conjurer", "glasses");
	public static final ModelLayerLocation THROWING_CARD = layer("throwing_card");

	public static final ModelLayerLocation CONJURER_HAT = layer("conjurer_hat");

	private static ModelLayerLocation layer(String name)
	{
		return layer(name, "main");
	}

	private static ModelLayerLocation layer(String name, String layer)
	{
		return new ModelLayerLocation(TheConjurerMod.locate(name), layer);
	}
}