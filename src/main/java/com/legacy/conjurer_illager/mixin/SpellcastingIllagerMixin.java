package com.legacy.conjurer_illager.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.legacy.conjurer_illager.entity.ConjurerEntity;

import net.minecraft.world.entity.monster.SpellcasterIllager;

@Mixin(SpellcasterIllager.class)
public class SpellcastingIllagerMixin
{
	@Inject(at = @At(value = "INVOKE_ASSIGN", target = "net/minecraft/world/entity/monster/SpellcasterIllager.getCurrentSpell()Lnet/minecraft/world/entity/monster/SpellcasterIllager$IllagerSpell;"), method = "tick()V", cancellable = true)
	private void tick(CallbackInfo callback)
	{
		if (((SpellcasterIllager) (Object) this) instanceof ConjurerEntity)
			callback.cancel();
	}
}
