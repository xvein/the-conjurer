package com.legacy.conjurer_illager.data;

import com.legacy.conjurer_illager.TheConjurerMod;
import com.legacy.structure_gel.api.data.providers.RegistrarDatapackEntriesProvider;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.data.DataGenerator;
import net.minecraftforge.data.event.GatherDataEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class ConjurerDataGen
{
	@SubscribeEvent
	public static void gatherData(GatherDataEvent event)
	{
		DataGenerator gen = event.getGenerator();
		boolean server = event.includeServer();

		RegistrarDatapackEntriesProvider provider = RegistrarHandler.createGenerator(gen.getPackOutput(), TheConjurerMod.MODID);
		gen.addProvider(server, provider);
	}
}
