package com.legacy.conjurer_illager.entity;

import javax.annotation.Nullable;

import com.legacy.conjurer_illager.IllagerRegistry;
import com.legacy.conjurer_illager.TheConjurerMod;
import com.legacy.conjurer_illager.registry.IllagerEntityTypes;

import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.damagesource.EntityDamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.animal.Rabbit;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.ThrowableProjectile;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.HitResult;
import net.minecraftforge.network.NetworkHooks;
import net.minecraftforge.network.PlayMessages;

public class ThrowingCardEntity extends ThrowableProjectile
{
	private static final EntityDataAccessor<Integer> CARD_TYPE = SynchedEntityData.<Integer>defineId(ThrowingCardEntity.class, EntityDataSerializers.INT);
	private static final DamageSource CARD_SOURCE = new DamageSource(TheConjurerMod.find("papercut")).setProjectile();

	public ThrowingCardEntity(EntityType<? extends ThrowingCardEntity> type, Level world)
	{
		super(type, world);
	}

	public ThrowingCardEntity(EntityType<? extends ThrowingCardEntity> type, LivingEntity thrower, Level world)
	{
		super(type, thrower, world);
	}

	public ThrowingCardEntity(PlayMessages.SpawnEntity spawnEntity, Level world)
	{
		this(IllagerEntityTypes.THROWING_CARD, world);
	}

	@Override
	protected void onHit(HitResult result)
	{
		HitResult.Type raytraceresult$type = result.getType();
		if (raytraceresult$type == HitResult.Type.BLOCK)
		{
			BlockHitResult traceResult = (BlockHitResult) result;
			BlockState blockstate = this.level.getBlockState(traceResult.getBlockPos());
			blockstate.onProjectileHit(this.level, blockstate, traceResult, this);

			if (this.getOwner()instanceof Player player && !player.isCreative())
				this.spawnAtLocation(IllagerRegistry.THROWING_CARD.get());

			this.discard();
		}

		if (raytraceresult$type == HitResult.Type.ENTITY)
		{
			Entity entity = ((EntityHitResult) result).getEntity();

			// getOwner = getThrower
			if (this.getOwner() != null && !entity.isAlliedTo(this.getOwner()) && !(entity instanceof Rabbit))
			{
				boolean magic = this.getOwner() instanceof ConjurerEntity;

				int i = magic ? 2 + this.level.getDifficulty().getId() : 1;

				if (!entity.isInvulnerable())
				{
					var source = getCardSource(this.getOwner());

					if (magic)
						source.setScalesWithDifficulty();

					entity.hurt(source, i);
					this.discard();
				}
			}
		}
	}

	@Override
	public void tick()
	{
		super.tick();

		if (this.tickCount % 8 == 0)
			this.level.addParticle(ParticleTypes.CRIT, this.getRandomX(0.5D), this.getRandomY(), this.getRandomZ(0.5D), this.random.nextGaussian() * 0.1D, 0.0D, this.random.nextGaussian() * 0.1D);
	}

	@Override
	public Packet<ClientGamePacketListener> getAddEntityPacket()
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);
		compound.putInt("CardType", this.getCardType());
	}

	@Override
	public void readAdditionalSaveData(CompoundTag compound)
	{
		super.readAdditionalSaveData(compound);
		this.setCardType(compound.getInt("CardType"));
	}

	@Override
	protected void defineSynchedData()
	{
		this.entityData.define(CARD_TYPE, 0);
	}

	public int getCardType()
	{
		return this.entityData.get(CARD_TYPE);
	}

	public void setCardType(int type)
	{
		this.entityData.set(CARD_TYPE, type);
	}

	private static DamageSource getCardSource(@Nullable Entity entity)
	{
		return entity != null ? new EntityDamageSource(TheConjurerMod.find("indirect_papercut"), entity).setProjectile() : CARD_SOURCE;
	}
}
