package com.legacy.conjurer_illager;

import org.apache.commons.lang3.tuple.Pair;

import net.minecraftforge.common.ForgeConfigSpec;

public class IllagerConfig
{
	public static final Common COMMON;
	public static final ForgeConfigSpec COMMON_SPEC;
	static
	{
		Pair<Common, ForgeConfigSpec> specPair = new ForgeConfigSpec.Builder().configure(Common::new);
		COMMON_SPEC = specPair.getRight();
		COMMON = specPair.getLeft();
	}

	public static class Common
	{
		/*public final StructureConfig theaterStructureConfig;*/

		public Common(ForgeConfigSpec.Builder builder)
		{
			/*this.theaterStructureConfig = StructureConfig.builder(builder, "Theater").placement(40, 35, 50).pushStructure().biomes("minecraft:dark_forest").popStructure().build();*/
		}
	}
}
