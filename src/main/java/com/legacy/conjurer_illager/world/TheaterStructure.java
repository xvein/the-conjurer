package com.legacy.conjurer_illager.world;

import java.util.Arrays;

import com.legacy.conjurer_illager.MLSupporter;
import com.legacy.conjurer_illager.TheConjurerMod;
import com.legacy.conjurer_illager.entity.ConjurerEntity;
import com.legacy.conjurer_illager.registry.IllagerEntityTypes;
import com.legacy.conjurer_illager.registry.IllagerJigsawTypes;
import com.legacy.conjurer_illager.registry.IllagerStructures;
import com.legacy.structure_gel.api.structure.jigsaw.ExtendedJigsawStructurePiece;
import com.legacy.structure_gel.api.structure.jigsaw.IPieceFactory;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawCapability.IJigsawCapability;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawCapability.JigsawType;
import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.StringTag;
import net.minecraft.network.chat.Component;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.animal.TropicalFish;
import net.minecraft.world.entity.monster.Pillager;
import net.minecraft.world.entity.npc.Villager;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.LecternBlockEntity;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePieceSerializationContext;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePieceType;

public class TheaterStructure
{
	public static class Capability implements IJigsawCapability
	{
		public static final Capability INSTANCE = new Capability();
		public static final Codec<Capability> CODEC = Codec.unit(INSTANCE);

		@Override
		public JigsawType<?> getType()
		{
			return IllagerJigsawTypes.THEATER;
		}

		@Override
		public IPieceFactory getPieceFactory()
		{
			return Piece::new;
		}
	}

	public static class Piece extends ExtendedJigsawStructurePiece
	{
		public Piece(IPieceFactory.Context context)
		{
			super(context);
		}

		public Piece(StructurePieceSerializationContext context, CompoundTag nbt)
		{
			super(context, nbt);
		}

		@Override
		public void handleDataMarker(String key, BlockPos pos, ServerLevelAccessor worldIn, RandomSource rand, BoundingBox bounds)
		{
			if (key.equals("conjurer"))
			{
				setAir(worldIn, pos);
				ConjurerEntity entity = createEntity(IllagerEntityTypes.CONJURER, worldIn, pos, this.rotation);
				entity.finalizeSpawn(worldIn, worldIn.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, null, null);
				entity.setPersistenceRequired();
				worldIn.addFreshEntity(entity);
			}
			else if (key.equals("admissioner"))
			{
				setAir(worldIn, pos);
				Pillager entity = createEntity(EntityType.PILLAGER, worldIn, pos, this.rotation);
				entity.finalizeSpawn(worldIn, worldIn.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, null, null);
				entity.setPersistenceRequired();
				worldIn.addFreshEntity(entity);
				
				entity.restrictTo(pos, 1);
			}
			else if (key.equals("villager"))
			{
				setAir(worldIn, pos);
				Villager entity = createEntity(EntityType.VILLAGER, worldIn, pos, this.rotation);
				entity.finalizeSpawn(worldIn, worldIn.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, null, null);
				worldIn.addFreshEntity(entity);

				entity.goalSelector.addGoal(9, new LookAtPlayerGoal(entity, ConjurerEntity.class, 15.0F, 2.0F));
			}
			else if (key.equals("audience"))
			{
				setAir(worldIn, pos);
				Mob entity = createEntity(EntityType.PILLAGER, worldIn, pos, this.rotation);

				if (rand.nextFloat() < 0.05F)
					entity = createEntity(EntityType.ILLUSIONER, worldIn, pos, this.rotation);
				else if (rand.nextFloat() < 0.40F)
					entity = createEntity(EntityType.VINDICATOR, worldIn, pos, this.rotation);
				else if (rand.nextFloat() < 0.20F)
					entity = createEntity(EntityType.WITCH, worldIn, pos, this.rotation);

				entity.finalizeSpawn(worldIn, worldIn.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, null, null);
				entity.setPersistenceRequired();
				worldIn.addFreshEntity(entity);

				entity.restrictTo(pos, 1);
				entity.goalSelector.addGoal(9, new LookAtPlayerGoal(entity, ConjurerEntity.class, 15.0F, 2.0F));
			}
			else if (key.contains("lectern"))
			{
				setAir(worldIn, pos);

				ItemStack bookStack = new ItemStack(Items.WRITTEN_BOOK);
				CompoundTag bookNBT = bookStack.getOrCreateTag();
				ListTag bookPages = new ListTag();

				String pageText = "VIP Admission List\n -=-=-=-=-=-=-=-=-\n";
				String[] names = TheConjurerMod.SUPPORTERS.getSupporters().values().stream().filter(mls -> mls.rank == MLSupporter.Rank.DEV || mls.rank == MLSupporter.Rank.SUPPORTER).map(mls -> mls.name).toArray(String[]::new);

				if (names.length > 0)
				{
					for (int i = 0; i < 12; i++)
					{
						String name = names[rand.nextInt(names.length)];
						pageText = pageText + name + "\n";
						names = Arrays.asList(names).stream().filter(s -> s != name).toArray(String[]::new);
					}
					bookPages.add(StringTag.valueOf(Component.Serializer.toJson(Component.literal(pageText))));

					bookNBT.putString("title", "Admission List");
					bookNBT.putString("author", "The Conjurer");
					bookNBT.put("pages", bookPages);

					bookStack.setTag(bookNBT);
					LecternBlockEntity lectern = (LecternBlockEntity) worldIn.getBlockEntity(pos.below());
					lectern.setBook(bookStack);
				}
			}
			else if (key.equals("fish"))
			{
				worldIn.setBlock(pos, Blocks.SANDSTONE.defaultBlockState(), 3);
				TropicalFish entity = createEntity(EntityType.TROPICAL_FISH, worldIn, pos.above(rand.nextInt(3) + 1), this.rotation);
				entity.finalizeSpawn(worldIn, worldIn.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, null, null);
				entity.setPersistenceRequired();
				worldIn.addFreshEntity(entity);
			}
		}

		@Override
		public StructurePieceType getType()
		{
			return IllagerStructures.THEATER.getPieceType().get();
		}
	}
}
