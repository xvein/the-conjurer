package com.legacy.conjurer_illager.registry;

import com.legacy.conjurer_illager.TheConjurerMod;
import com.legacy.conjurer_illager.world.TheaterStructure;
import com.legacy.structure_gel.api.events.RegisterJigsawCapabilityEvent;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawCapability.JigsawType;

import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = TheConjurerMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class IllagerJigsawTypes
{
	public static final JigsawType<TheaterStructure.Capability> THEATER = () -> TheaterStructure.Capability.CODEC;

	@SubscribeEvent
	protected static void init(final RegisterJigsawCapabilityEvent event)
	{
		register(event, "theater", THEATER);
	}

	private static void register(RegisterJigsawCapabilityEvent event, String key, JigsawType<?> type)
	{
		event.register(TheConjurerMod.locate(key), type);
	}
}
