package com.legacy.conjurer_illager.registry;

import java.util.List;
import java.util.function.Supplier;

import com.legacy.conjurer_illager.TheConjurerMod;
import com.legacy.structure_gel.api.registry.registrar.Registrar;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.levelgen.structure.templatesystem.BlockMatchTest;
import net.minecraft.world.level.levelgen.structure.templatesystem.ProcessorRule;
import net.minecraft.world.level.levelgen.structure.templatesystem.RuleProcessor;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureProcessorList;

public class IllagerProcessors
{
	public static final RegistrarHandler<StructureProcessorList> HANDLER = RegistrarHandler.getOrCreate(Registries.PROCESSOR_LIST, TheConjurerMod.MODID);

	public static final Registrar.Pointer<StructureProcessorList> COBBLE_WALLS_TO_GRASS = register("cobble_walls_to_grass", () -> new StructureProcessorList(List.of(new RuleProcessor(List.of(new ProcessorRule(new BlockMatchTest(Blocks.COBBLESTONE_WALL), new BlockMatchTest(Blocks.GRASS_BLOCK), Blocks.GRASS_BLOCK.defaultBlockState()), new ProcessorRule(new BlockMatchTest(Blocks.COBBLESTONE_WALL), new BlockMatchTest(Blocks.DIRT), Blocks.DIRT.defaultBlockState()), new ProcessorRule(new BlockMatchTest(Blocks.COBBLESTONE_WALL), new BlockMatchTest(Blocks.STONE), Blocks.STONE.defaultBlockState()))))));

	private static Registrar.Pointer<StructureProcessorList> register(String key, Supplier<StructureProcessorList> processorList)
	{
		return HANDLER.createPointer(key, processorList);
	}

}
