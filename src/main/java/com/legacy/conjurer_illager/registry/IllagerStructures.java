package com.legacy.conjurer_illager.registry;

import com.legacy.conjurer_illager.TheConjurerMod;
import com.legacy.conjurer_illager.world.TheaterPools;
import com.legacy.conjurer_illager.world.TheaterStructure;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;
import com.legacy.structure_gel.api.registry.registrar.StructureRegistrar;
import com.legacy.structure_gel.api.structure.ExtendedJigsawStructure;
import com.legacy.structure_gel.api.structure.GridStructurePlacement;

import net.minecraft.core.registries.Registries;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.levelgen.structure.Structure;
import net.minecraft.world.level.levelgen.structure.StructureSpawnOverride;
import net.minecraft.world.level.levelgen.structure.TerrainAdjustment;
import net.minecraft.world.level.levelgen.structure.pools.StructureTemplatePool;

public class IllagerStructures
{
	public static final RegistrarHandler<StructureTemplatePool> HANDLER = RegistrarHandler.getOrCreate(Registries.TEMPLATE_POOL, TheConjurerMod.MODID).bootstrap(TheaterPools::bootstrap);

	// 257857
	// wish I could rename this without breakinge lots of things
	// @formatter:off
	/*public static final StructureRegistrar<ExtendedJigsawStructure> THEATER2 = StructureRegistrar.jigsawBuilder(TheConjurerMod.locate("theatre"))
			.addPiece(() -> TheaterStructure.Piece::new)
			.pushStructure(s -> new ExtendedJigsawStructure(s, TheaterPools.ROOT, 7, ConstantHeight.of(VerticalAnchor.absolute(0)), false, Heightmap.Types.WORLD_SURFACE_WG).withPieceFactory(IllagerStructures.THEATER.getRegistryName(), TheaterStructure.Piece::new))
			.terrainAdjustment(TerrainAdjustment.BEARD_THIN).noSpawns(StructureSpawnOverride.BoundingBoxType.PIECE).config(() -> IllagerConfig.COMMON.theaterStructureConfig.getStructure())
			.popStructure()
			.placement(() -> GridStructurePlacement.builder().config(() -> IllagerConfig.COMMON.theaterStructureConfig).build(IllagerStructures.THEATER.getRegistryName()))
			.build();*/
	
	public static final StructureRegistrar<ExtendedJigsawStructure> THEATER = StructureRegistrar.jigsawBuilder(TheConjurerMod.locate("theatre"))
			.addPiece(() -> TheaterStructure.Piece::new)
			.pushStructure((c, s) -> ExtendedJigsawStructure.builder(s, c.lookup(Registries.TEMPLATE_POOL).getOrThrow(TheaterPools.ROOT)).heightmap(Heightmap.Types.WORLD_SURFACE_WG).capability(TheaterStructure.Capability.INSTANCE).build())
				.terrainAdjustment(TerrainAdjustment.BEARD_THIN).noSpawns(StructureSpawnOverride.BoundingBoxType.PIECE).biomes(Biomes.DARK_FOREST)
			.popStructure()
			.placement(() -> GridStructurePlacement.builder(40, 35, 0.50F).build(IllagerStructures.THEATER.getRegistryName()))
			.build();
	// @formatter:on

	public static final TagKey<Structure> THEATERS = TagKey.create(Registries.STRUCTURE, TheConjurerMod.locate("on_theater_maps"));

	public static void init()
	{
	}
}