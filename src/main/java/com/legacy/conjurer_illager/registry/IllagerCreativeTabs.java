package com.legacy.conjurer_illager.registry;

import com.legacy.conjurer_illager.IllagerRegistry;
import com.legacy.conjurer_illager.TheConjurerMod;

import net.minecraft.world.item.CreativeModeTabs;
import net.minecraftforge.event.CreativeModeTabEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = TheConjurerMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class IllagerCreativeTabs
{
	@SubscribeEvent
	public static void modifyExisting(CreativeModeTabEvent.BuildContents event)
	{
		if (event.getTab() == CreativeModeTabs.SPAWN_EGGS)
			event.accept(IllagerRegistry.CONJURER_SPAWN_EGG);

		if (event.getTab() == CreativeModeTabs.COMBAT)
		{
			event.accept(IllagerRegistry.CONJURER_HAT);
			event.accept(IllagerRegistry.THROWABLE_BALL);
			event.accept(IllagerRegistry.THROWING_CARD);
		}

		if (event.getTab() == CreativeModeTabs.TOOLS_AND_UTILITIES)
			event.accept(IllagerRegistry.DELVE_DEEPER_RECORD);
	}
}
