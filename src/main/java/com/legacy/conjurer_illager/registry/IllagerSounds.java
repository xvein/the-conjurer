package com.legacy.conjurer_illager.registry;

import com.legacy.conjurer_illager.TheConjurerMod;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraftforge.registries.ForgeRegistries;

public class IllagerSounds
{
	public static final SoundEvent ENTITY_CONJURER_IDLE = create("entity.conjurer.idle");
	public static final SoundEvent ENTITY_CONJURER_HURT = create("entity.conjurer.hurt");
	public static final SoundEvent ENTITY_CONJURER_DEATH = create("entity.conjurer.death");
	public static final SoundEvent ENTITY_CONJURER_CELEBRATE = create("entity.conjurer.celebrate");

	public static final SoundEvent ENTITY_CONJURER_CAST_SPELL = create("entity.conjurer.cast_spell");
	public static final SoundEvent ENTITY_CONJURER_DISAPPEAR = create("entity.conjurer.disappear");
	public static final SoundEvent ENTITY_CONJURER_PREPARE_VANISH = create("entity.conjurer.prepare_vanish");
	public static final SoundEvent ENTITY_CONJURER_PREPARE_RABBIT = create("entity.conjurer.prepare_rabbit");
	public static final SoundEvent ENTITY_CONJURER_PREPARE_ATTACK = create("entity.conjurer.prepare_attack");
	public static final SoundEvent ENTITY_CONJURER_PREPARE_DISPLACEMENT = create("entity.conjurer.prepare_displacement");

	public static final SoundEvent ENTITY_BOUNCY_BALL_BOUNCE = create("entity.bouncy_ball.bounce");
	public static final SoundEvent ENTITY_BOUNCY_BALL_VANISH = create("entity.bouncy_ball.vanish");

	/*public static final SoundEvent RECORD_CONJURED_CHAOS = create("records.conjured_chaos");*/
	public static final SoundEvent RECORD_DELVE_DEEPER = create("records.delve_deeper");

	public static void init()
	{
	}

	private static SoundEvent create(String name)
	{
		ResourceLocation location = TheConjurerMod.locate(name);
		SoundEvent sound = SoundEvent.createVariableRangeEvent(location);

		ForgeRegistries.SOUND_EVENTS.register(location, sound);
		return sound;
	}

}