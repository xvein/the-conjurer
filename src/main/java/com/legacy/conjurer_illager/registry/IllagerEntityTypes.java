package com.legacy.conjurer_illager.registry;

import com.legacy.conjurer_illager.TheConjurerMod;
import com.legacy.conjurer_illager.entity.BouncingBallEntity;
import com.legacy.conjurer_illager.entity.ConjurerEntity;
import com.legacy.conjurer_illager.entity.ThrowingCardEntity;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.entity.SpawnPlacements;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraftforge.event.entity.EntityAttributeCreationEvent;
import net.minecraftforge.event.entity.SpawnPlacementRegisterEvent;
import net.minecraftforge.event.entity.SpawnPlacementRegisterEvent.Operation;
import net.minecraftforge.registries.RegisterEvent;

public class IllagerEntityTypes
{
	public static final EntityType<ConjurerEntity> CONJURER = buildEntity("conjurer", EntityType.Builder.of(ConjurerEntity::new, MobCategory.MONSTER).sized(0.6F, 1.95F));
	public static final EntityType<BouncingBallEntity> BOUNCING_BALL = buildEntity("bouncing_ball", EntityType.Builder.<BouncingBallEntity>of(BouncingBallEntity::new, MobCategory.MISC).setCustomClientFactory(BouncingBallEntity::new).fireImmune().setShouldReceiveVelocityUpdates(true).sized(0.4F, 0.4F));
	public static final EntityType<ThrowingCardEntity> THROWING_CARD = buildEntity("throwing_card", EntityType.Builder.<ThrowingCardEntity>of(ThrowingCardEntity::new, MobCategory.MISC).setCustomClientFactory(ThrowingCardEntity::new).fireImmune().setShouldReceiveVelocityUpdates(true).sized(0.3F, 0.1F));

	private static RegisterEvent registerEvent;

	public static void init(RegisterEvent event)
	{
		registerEvent = event;

		register("conjurer", CONJURER);
		register("bouncing_ball", BOUNCING_BALL);
		register("throwing_card", THROWING_CARD);
	}

	private static void register(String name, EntityType<?> type)
	{
		if (registerEvent == null)
			return;

		registerEvent.register(Registries.ENTITY_TYPE, TheConjurerMod.locate(name), () -> type);
	}

	public static void onAttributesRegistered(EntityAttributeCreationEvent event)
	{
		event.put(IllagerEntityTypes.CONJURER, ConjurerEntity.registerAttributes().build());
	}

	private static <T extends Entity> EntityType<T> buildEntity(String key, EntityType.Builder<T> builder)
	{
		return builder.build(TheConjurerMod.find(key));
	}

	public static void registerPlacements(SpawnPlacementRegisterEvent event)
	{
		event.register(IllagerEntityTypes.CONJURER, SpawnPlacements.Type.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, ConjurerEntity::checkMobSpawnRules, Operation.REPLACE);
	}
}
